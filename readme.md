# VMPOC Persistent Volumes

Contains all peristent volumes used for Jenkins in VMPOC.

## Current list of volumes

- Maven repository volume ([k8-maven-volume.yaml](k8-maven-volume.yaml)).

## Maven repository volume

Persistent volume to store Maven repository to improve build time.

### Usage

```yaml
volumes:
  - name: maven-repo
    persistentVolumeClaim:
      claimName: maven-repo-pv-claim
```
